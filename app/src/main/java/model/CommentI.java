package model;

import java.util.Date;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface CommentI {
    public Long getId();
    public void setId(Long id);
    public Integer getScore();
    public void setScore(Integer score);
    public String getText();
    public void setText(String text);
    public Date getCreationDate();
    public void setCreationDate(Date creationDate);
    public UserI getUser();
    public void setUser(UserI user);
    public PostI getPost();
    public void setPost(PostI post);

}
