package model;

import java.util.Date;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface VoteI {
    public Long getId();
    public void setId(Long id);
    public String getVoteTypeId();
    public void setVoteTypeId(String voteTypeId);
    public Date getCreationDate();
    public void setCreationDate(Date creationDate);
    public UserI getUser();
    public void setUser(UserI user);
    public PostI getPost();
    public void setPost(PostI post);
}
