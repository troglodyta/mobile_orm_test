package model;

import java.util.Date;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface BadgeI {

    public Long getId();
    public void setId(Long id);
    public String getName();
    public void setName(String name);
    public Date getDate();
    public void setDate(Date date);
    public String getBadgeClass();
    public void setBadgeClass(String badgeClass);
    public Boolean getTagBased();
    public void setTagBased(Boolean tagBased);
    public UserI getUser();
    public void setUser(UserI user);



}
