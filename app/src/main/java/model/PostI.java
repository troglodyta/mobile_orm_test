package model;

import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface PostI {

    public Long getId();
    public void setId(Long id);
    public Long getPostTypeId();
    public void setPostTypeId(Long postTypeId);
    public Long getAcceptedAnswerId();
    public void setAcceptedAnswerId(Long acceptedAnswerId);
    public Date getCreationDate();
    public void setCreationDate(Date creationDate);
    public Integer getScore();
    public void setScore(Integer score);
    public Integer getViewCount();
    public void setViewCount(Integer viewCount);
    public String getBody();
    public void setBody(String body);
    public String getOwnerDisplayName();
    public void setOwnerDisplayName(String ownerDisplayName);
    public String getLastEditorDisplayName();
    public void setLastEditorDisplayName(String lastEditorDisplayName);
    public Date getLastEditDate();
    public void setLastEditDate(Date lastEditDate);
    public Date getLastActivityDate();
    public void setLastActivityDate(Date lastActivityDate);
    public String getTitle();
    public void setTitle(String title);
    public String getTags();
    public void setTags(String tags);
    public Integer getAnswerCount();
    public void setAnswerCount(Integer answerCount);
    public Integer getCommentCount();
    public void setCommentCount(Integer commentCount);
    public Integer getFavoriteCount();
    public void setFavoriteCount(Integer favoriteCount);
    public Date getClosedDate();
    public void setClosedDate(Date closedDate);
    public Date getCommunityOwnedDate();
    public void setCommunityOwnedDate(Date communityOwnedDate);
    public Long getParentId();
    public void setParentId(Long parentId);
    public UserI getOwnerUser();
    public void setOwnerUser(UserI ownerUser);
    public UserI getLastEditorUser();
    public void setLastEditorUser(UserI lastEditorUser);
    public List<CommentI> getComments();
    public List<VoteI> getVotes();

}
