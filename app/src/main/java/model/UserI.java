package model;

import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface UserI {
    public Long getId();
    public void setId(Long id);
    public Integer getReputation();
    public void setReputation(Integer reputation);
    public Date getCreationDate();
    public void setCreationDate(Date creationDate);
    public String getDisplayName();
    public void setDisplayName(String displayName);
    public Date getLastAccessDate();
    public void setLastAccessDate(Date lastAccessDate);
    public String getWebsiteUrl();
    public void setWebsiteUrl(String websiteUrl);
    public String getLocation();
    public void setLocation(String location);
    public String getAboutMe();
    public void setAboutMe(String aboutMe);
    public String getViews();
    public void setViews(String views);
    public Integer getUpVotes();
    public void setUpVotes(Integer upVotes);
    public Integer getDownVotes();
    public void setDownVotes(Integer downVotes);
    public Integer getAge();
    public void setAge(Integer age);
    public Long getAccountId();
    public void setAccountId(Long accountId);
    public List<BadgeI> getBadges();
}
