package test;

import android.content.Context;

import java.util.List;

import dao.DaoI;
import dao.DaoManager;

/**
 * Created by Daniel on 05.05.2016.
 */
public class UpdateTestCase extends AbstractModTestCase<UpdateTest> {
    public UpdateTestCase(List<Integer> rowToModify, List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex) {
        super(new UpdateTest(), rowToModify, dbNames, orms, runCount, contex);
    }

    @Override
    public DaoI getDaoI() {
        return DaoManager.INSTANCE.getCommentDao();
    }
}
