package test;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dao.DaoManager;

/**
 * Created by Daniel on 05.05.2016.
 */
public abstract class AbstractModTestCase<T extends TestI> extends AbstractTestCase<T> {

    private List<Integer> rowsToModify;

    public AbstractModTestCase(T test, List<Integer> rowToModify,List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex) {
        super(test, dbNames, orms, runCount, contex, false);
        this.rowsToModify = rowToModify;
    }


    @Override
    public Map<DaoManager.OrmType, List<Result>> run(){
        results = new HashMap<>();

        for(DaoManager.OrmType orm: orms){
            List<Result> executions = new ArrayList<>();
            String dbName = dbNames.get(0);
            for(int i = 0;i < rowsToModify.size(); i++){
                int rowsToModif = rowsToModify.get(i);
                DaoManager.INSTANCE.createDaoConnection(orm, dbName, context, false);
                ((ModifTest)test).setRows(rowsToModif);
                Result res = test.run(runCount, getDaoI());
                Log.d("Execute", orm + " : " + dbName);
                executions.add(res);
            }
            results.put(orm, executions);
        }
        return results;
    }


    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        List<String> rows = new ArrayList<>();
        List<List<Result>> resListList = new ArrayList<>();
        String header ="Rows;";
        for(DaoManager.OrmType type : orms) {
            List<Result> rList = results.get(type);
            resListList.add(rList);
            header += type+"_AVG;"+type+"_SD;";
        }

        for(int i = 0; i < rowsToModify.size();i++){
            rows.add("");
        }

        for(int i = 0; i < rowsToModify.size();i++){
            // for(List<Result> rl : resListList) {
            List<Result> r0 = resListList.get(0);
            String s = rows.get(i);
            rows.set(i, s + r0.get(i).getRowNumber() + ";");
            // }
        }

        for(int i = 0; i < rowsToModify.size();i++){
            for(List<Result> rl : resListList) {
                String s = rows.get(i);
                rows.set(i, s + stringFromDouble(rl.get(i).getAvgTime()) + ";"+stringFromDouble(rl.get(i).getSdTime())+";");
            }
        }

        rows.add(0,header);
        for(String row : rows) {
            sb.append(row + "\n");
        }
        return sb.toString();
    }
}
