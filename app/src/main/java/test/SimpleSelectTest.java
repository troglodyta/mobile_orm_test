package test;

import java.sql.SQLException;

import dao.PostDaoI;

/**
 * Created by Daniel on 04.05.2016.
 */
public class SimpleSelectTest extends  AbstractTest<PostDaoI> {
    @Override
    public void runQuery(PostDaoI dao) {
        try {
            dao.simpleSelect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
