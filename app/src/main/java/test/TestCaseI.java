package test;

import java.util.List;
import java.util.Map;

import dao.DaoManager;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface TestCaseI<T extends TestI> {

    String getName();
    Map<DaoManager.OrmType, List<Result>> run();
    void saveToFile();
}
