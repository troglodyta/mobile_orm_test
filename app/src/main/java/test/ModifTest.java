package test;

/**
 * Created by Daniel on 05.05.2016.
 */
public interface ModifTest {

    void setRows(int rows);

    int getRows();
}
