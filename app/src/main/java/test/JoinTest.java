package test;

import java.sql.SQLException;

import dao.PostDaoI;

/**
 * Created by Daniel on 05.05.2016.
 */
public class JoinTest extends  AbstractTest<PostDaoI> {
    @Override
    public void runQuery(PostDaoI dao) {
        try {
            dao.threeTableJoin();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
