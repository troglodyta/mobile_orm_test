package test;

import android.content.Context;

import java.util.List;

import dao.DaoI;
import dao.DaoManager;

/**
 * Created by Daniel on 05.05.2016.
 */
public class JoinTestCase extends  AbstractTestCase<JoinTest> {
    public JoinTestCase( List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex, boolean cache) {
        super(new JoinTest(), dbNames, orms, runCount, contex, cache);
    }

    @Override
    public DaoI getDaoI() {
        return DaoManager.INSTANCE.getPostDao();
    }
}
