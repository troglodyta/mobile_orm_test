package test;

import java.sql.SQLException;

import dao.DaoManager;
import dao.PostDaoI;
import model.PostI;

/**
 * Created by Daniel on 03.05.2016.
 */
public class PostByIdTest extends AbstractTest<PostDaoI> {

    @Override
    public void runQuery(PostDaoI dao) {
        try {
           long id = (long) (Math.random()*1000);
            dao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
