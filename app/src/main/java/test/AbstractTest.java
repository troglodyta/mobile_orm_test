package test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dao.DaoI;

/**
 * Created by Daniel on 03.05.2016.
 */
public abstract class AbstractTest<T extends DaoI> implements TestI<T> {
    @Override
    public Result run(int times, T dao) {
        List<Double> executionTimes = new ArrayList<>(times);

        long rowNumber = -1;
        try {
            if(this instanceof ModifTest){
                rowNumber = ((ModifTest) this).getRows();
            }
            else {
                rowNumber = dao.rowCount();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for(int i =0; i < times; i++) {
            double startTime = System.currentTimeMillis();
            runQuery(dao);
            double endTime = System.currentTimeMillis();
            executionTimes.add(endTime - startTime);
        }

        double avg = getMean(executionTimes);
        double sd = getStdDev(executionTimes);
        return new Result(rowNumber,round(avg,2),round(sd,2));
    }

    double getMean(List<Double> data)
    {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/data.size();
    }

    double getVariance(List<Double> data)
    {
        double mean = getMean(data);
        double temp = 0;
        for(double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.size();
    }


    double getStdDev(List<Double> data)
    {
        return Math.sqrt(getVariance(data));
    }

    public double median(List<Double> dataL)
    {
        Double[] data = dataL.toArray(new Double[dataL.size()]);
        Arrays.sort(data);

        if (data.length % 2 == 0)
        {
            return (data[(data.length / 2) - 1] + data[data.length / 2]) / 2.0;
        }
        else
        {
            return data[data.length / 2];
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


}


