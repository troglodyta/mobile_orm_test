package test;

import dao.DaoI;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface TestI<T extends DaoI> {

    public Result run(int times,T dao);

    public void runQuery(T dao);
}
