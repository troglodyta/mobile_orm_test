package test;

import java.sql.SQLException;

import dao.CommentDaoI;

/**
 * Created by Daniel on 05.05.2016.
 */
public class DeleteTest extends AbstractTest<CommentDaoI> implements ModifTest {
    int currentId = 1;
    int rows;

    @Override
    public void runQuery(CommentDaoI dao) {
        try {
            dao.delete(currentId, currentId + rows);
            currentId += rows +1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    @Override
    public int getRows() {
        return rows;
    }
}
