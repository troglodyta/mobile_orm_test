package test;

import android.content.Context;

import java.util.List;

import dao.DaoI;
import dao.DaoManager;

/**
 * Created by Daniel on 05.05.2016.
 */
public class InstertTestCase extends AbstractModTestCase<InstertTest> {
    public InstertTestCase(List<Integer> rowToModify, List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex) {
        super(new InstertTest(), rowToModify, dbNames, orms, runCount, contex);
    }

    @Override
    public DaoI getDaoI() {
        return DaoManager.INSTANCE.getCommentDao();
    }
}
