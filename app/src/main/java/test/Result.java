package test;

/**
 * Created by Daniel on 03.05.2016.
 */
public class Result {
    private long rowNumber;
    private double avgTime;
    private double sdTime;

    public Result(long rowNumber, double avgTime, double sdTime) {
        this.rowNumber = rowNumber;
        this.avgTime = avgTime;
        this.sdTime = sdTime;
    }

    public long getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(long rowNumber) {
        this.rowNumber = rowNumber;
    }

    public double getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(double avgTime) {
        this.avgTime = avgTime;
    }

    public double getSdTime() {
        return sdTime;
    }

    public void setSdTime(double sdTime) {
        this.sdTime = sdTime;
    }

    @Override
    public String toString() {
        return "Result{" +
                "rowNumber=" + rowNumber +
                ", avg=" + avgTime +
                '}';
    }
}
