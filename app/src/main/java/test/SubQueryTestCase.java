package test;

import android.content.Context;

import java.util.List;

import dao.DaoI;
import dao.DaoManager;

/**
 * Created by Daniel on 11.06.2016.
 */
public class SubQueryTestCase extends AbstractTestCase<SubQueryTest> {
    public SubQueryTestCase(List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex, boolean cache) {
        super(new SubQueryTest(), dbNames, orms, runCount, contex, cache);
    }

    public DaoI getDaoI() {
        return DaoManager.INSTANCE.getPostDao();
    }
}
