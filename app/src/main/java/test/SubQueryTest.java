package test;

import java.sql.SQLException;

import dao.PostDaoI;

/**
 * Created by Daniel on 11.06.2016.
 */
public class SubQueryTest extends  AbstractTest<PostDaoI>  {


        @Override
        public void runQuery(PostDaoI dao) {
            try {
                dao.subQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

}
