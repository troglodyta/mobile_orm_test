package test;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dao.DaoI;
import dao.DaoManager;

/**
 * Created by Daniel on 03.05.2016.
 */
public abstract class AbstractTestCase<T extends TestI> implements TestCaseI<T> {
    protected T test;
    protected List<String> dbNames;
    protected List<DaoManager.OrmType> orms;
    protected Map<DaoManager.OrmType, List<Result>> results;
    protected int runCount;
    protected Context context;
    protected boolean cache;

    public AbstractTestCase(T test,List<String> dbNames, List<DaoManager.OrmType> orms, int runCount, Context contex, boolean cache){
        this.test = test;
        this.dbNames = dbNames;
        this.orms = orms;
        this.runCount = runCount;
        this.context = contex;
        this.cache = cache;
    }


    @Override
    public Map<DaoManager.OrmType, List<Result>> run(){
        results = new HashMap<>();

        for(DaoManager.OrmType orm: orms){
            List<Result> executions = new ArrayList<>();
            for(int i = 0;i < dbNames.size(); i++){
                String dbName = dbNames.get(i);
                DaoManager.INSTANCE.createDaoConnection(orm, dbName, context, cache);

                    Result res = test.run(runCount, getDaoI());
                    Log.d("Execute",orm+" : "+dbName);
                    executions.add(res);
            }
            results.put(orm, executions);
        }
        return results;
    }

    public abstract DaoI getDaoI();

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public void saveToFile() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        String filename = getName()+"_"+runCount+"_"+".csv";
        Log.d("DIR",filename);
        File file = new File(dir, filename);
        try {
            PrintWriter pw = new PrintWriter(file);
            pw.write(this.toString());
            pw.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    };

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        List<String> rows = new ArrayList<>();
        List<List<Result>> resListList = new ArrayList<>();
        String header ="Rows;";
        for(DaoManager.OrmType type : orms) {
            List<Result> rList = results.get(type);
            resListList.add(rList);
            header += type+"_AVG;"+type+"_SD;";
        }

        for(int i = 0; i < dbNames.size();i++){
            rows.add("");
        }

        for(int i = 0; i < dbNames.size();i++){
            // for(List<Result> rl : resListList) {
            List<Result> r0 = resListList.get(0);
            String s = rows.get(i);
            rows.set(i, s + r0.get(i).getRowNumber() + ";");
            // }
        }

        for(int i = 0; i < dbNames.size();i++){
            for(List<Result> rl : resListList) {
                String s = rows.get(i);
                rows.set(i, s + stringFromDouble(rl.get(i).getAvgTime()) + ";"+stringFromDouble(rl.get(i).getSdTime())+";");
            }
        }

        rows.add(0,header);
        for(String row : rows) {
            sb.append(row+"\n");
        }
        return sb.toString();
    }

    protected String stringFromDouble(double d) {
        return (d+"").replaceAll("\\.",",");
    }
}
