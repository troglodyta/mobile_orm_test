package ormlite;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.PostI;
import model.UserI;
import model.VoteI;

@DatabaseTable(tableName = "votes")
@Data
@ToString(exclude="post")
@EqualsAndHashCode(exclude={"post"})
public class Vote implements VoteI{

	@DatabaseField(columnName = "Id", id = true, generatedId = false)
	private Long id;
	
	@DatabaseField(columnName = "VoteTypeId")
	private String voteTypeId;
	
	@DatabaseField(columnName = "CreationDate", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date creationDate;
	
	@DatabaseField(columnName = "UserId", canBeNull = true, foreign = true)
	private User user;
	
	@DatabaseField(columnName = "PostId", canBeNull = false, foreign = true)
	private Post post;

	@Override
	public UserI getUser() {
		return user;
	}

	@Override
	public void setUser(UserI user) {
		this.user = (User) user;
	}

	@Override
	public PostI getPost() {
		return post;
	}

	@Override
	public void setPost(PostI post) {
		this.post = (Post) post;
	}


	@Override
	public String toString() {
		String userS = user == null? null : getUser().getId()+"";
		String postS = post == null ? null : getPost().getId()+"";

		return "Vote{" +
				"id=" + id +
				", voteTypeId='" + voteTypeId + '\'' +
				", creationDate=" + creationDate +
				", user=" + userS +
				", post=" + postS +
				'}';
	}
}
