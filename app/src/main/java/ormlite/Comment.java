package ormlite;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.CommentI;
import model.PostI;
import model.UserI;

@DatabaseTable(tableName = "comments")
@Data
@ToString(exclude="post")
@EqualsAndHashCode(exclude={"post"})
public class Comment implements CommentI {

	@DatabaseField(columnName = "Id",id = true, generatedId = false)
	private Long id;
	
	@DatabaseField(columnName = "Score")
	private Integer score;
	
	@DatabaseField(columnName = "Text")
	private String text;
	
	@DatabaseField(columnName = "CreationDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date creationDate;
	
	@DatabaseField(columnName = "UserId", canBeNull = true, foreign = true)
	private User user;
	
	@DatabaseField(columnName = "PostId", canBeNull = true, foreign = true)
	private Post post;

	@Override
	public UserI getUser() {
		return user;
	}

	@Override
	public void setUser(UserI user) {
		this.user = (User) user;
	}

	@Override
	public PostI getPost() {
		return post;
	}

	@Override
	public void setPost(PostI post) {
		this.post = (Post) post;
	}


	@Override
	public String toString() {

		String userS = user == null? null : getUser().getId()+"";
		String postS = post == null ? null : getPost().getId()+"";
		return "Comment{" +
				"id=" + id +
				", score=" + score +
				", text='" + text + '\'' +
				", creationDate=" + creationDate +
				", user=" + userS +
				", post=" + postS +
				'}';
	}
}
