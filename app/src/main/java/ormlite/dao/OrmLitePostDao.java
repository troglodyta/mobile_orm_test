package ormlite.dao;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.DatabaseConnection;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dao.PostDaoI;
import datainit.DatabaseHelper;
import model.PostI;
import ormlite.Badge;
import ormlite.Comment;
import ormlite.Post;
import ormlite.User;

/**
 * Created by Daniel on 03.05.2016.
 */
public class OrmLitePostDao implements PostDaoI {
    private DatabaseHelper helper;
    private Dao<Post,String> postDao;
    private Dao<User,String> userDao;
    private Dao<Badge,String> badgeDao;
    private Dao<Comment, String> commentDao;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    private Date simpleSelectDate;
    private boolean cache;



    public OrmLitePostDao(DatabaseHelper helper, boolean cache){
        this.helper = helper;
        try {
            postDao = helper.getDao(Post.class);
            postDao.setObjectCache(cache);
            userDao = helper.getDao(User.class);
            userDao.setObjectCache(cache);
            badgeDao = helper.getDao(Badge.class);
            badgeDao.setObjectCache(cache);
            commentDao = helper.getDao(Comment.class);
            commentDao.setObjectCache(cache);
            simpleSelectDate = df.parse("2010-01-01");
            this.cache = cache;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }



    @Override
    public PostI getById(Long id) throws SQLException {
        PostI post = postDao.queryForId(id+"");
        return post;
    }

    @Override
    public long rowCount() throws SQLException {
        return postDao.countOf();
    }

    @Override
    public List<? extends PostI> simpleSelect() throws SQLException {
        QueryBuilder<Post, String> qb = postDao.queryBuilder();
            qb.where()
                .gt("lastActivityDate", simpleSelectDate)
                .and().eq("postTypeId",1)
                .and().like("body", "%java%");
        //Log.d("ST", qb.prepareStatementString());

        return qb.query();
    }

    @Override
    public List<? extends PostI> threeTableJoin() throws SQLException {
        QueryBuilder<Post, String> postQb = postDao.queryBuilder();
        QueryBuilder<User, String> userQb = userDao.queryBuilder();

        QueryBuilder<Badge, String> badgeQb = badgeDao.queryBuilder();
        badgeQb.where().eq("name", "Scholar");




        QueryBuilder<Post, String> resQb = postQb.join(userQb.join(badgeQb)).orderBy("creationDate",true);

        //Log.d("ST", resQb.prepareStatementString());
        List<Post> list = resQb.query();
        //Log.d("ST", list.size()+"");

        return list;
    }

  //  @Override
    public List<? extends PostI> subQuery() throws SQLException {
        QueryBuilder<Post, String> postQb = postDao.queryBuilder();
        QueryBuilder<User, String> userQb = userDao.queryBuilder();

        userQb.where().gt("age",0).and().lt("age", 25);
        userQb.selectColumns("Id");
        postQb.where().in("LastEditorUserId", userQb);
      //  Log.d("ST", postQb.prepareStatementString());

        List<Post> list = postQb.query();
     //   Log.d("ST", postQb.prepareStatementString());

        return list;
    }

    @Override
    public List<? extends PostI> limitQuery() throws SQLException {
        QueryBuilder<Post, String> qb = postDao.queryBuilder();
        qb.where()
                .eq("postTypeId",2);
        qb.limit(100);

         Log.d("ST", qb.prepareStatementString());

        List<Post> list = qb.query();

        return list;

    }


    public void insert(long minId, long maxId) throws SQLException {
        DatabaseConnection conn = commentDao.startThreadConnection();
        Savepoint savePoint = null;
        Date now = new Date();
        try {
            savePoint = conn.setSavePoint(null);
            for(long i = minId; i<=maxId; i++){
                Comment c = new Comment();
                c.setId(i);
                c.setCreationDate(now);
                c.setScore(30);
                c.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quam risus, aliquam in nunc vel, ultricies volutpat tortor. Sed efficitur, nisl posuere molestie commodo, mauris mi lacinia orci, sit amet gravida nisl orci vel est.");
                commentDao.create(c);
            }

        } finally {
            // commit at the end
            conn.commit(savePoint);
            commentDao.endThreadConnection(conn);
        }
    }
}
