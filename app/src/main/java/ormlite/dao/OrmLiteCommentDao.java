package ormlite.dao;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.DatabaseConnection;

import java.sql.SQLException;
import java.sql.Savepoint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import dao.CommentDaoI;
import datainit.DatabaseHelper;
import model.CommentI;
import ormlite.Badge;
import ormlite.Comment;
import ormlite.Post;
import ormlite.User;

/**
 * Created by Daniel on 05.05.2016.
 */
public class OrmLiteCommentDao implements CommentDaoI {

    private DatabaseHelper helper;
    private Dao<Comment, String> commentDao;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    private Date simpleSelectDate;



    public OrmLiteCommentDao(DatabaseHelper helper, boolean cache){
        this.helper = helper;
        try {
            commentDao = helper.getDao(Comment.class);
            commentDao.setObjectCache(cache);
            simpleSelectDate = df.parse("2010-01-01");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void insert(long minId, long maxId) throws SQLException {
        DatabaseConnection conn = commentDao.startThreadConnection();
        Savepoint savePoint = null;
        Date now = new Date();
        try {
            savePoint = conn.setSavePoint(null);
            for(long i = minId; i<maxId; i++){
                Comment c = new Comment();
                c.setId(i);
                c.setCreationDate(now);
                c.setScore(30);
                c.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quam risus, aliquam in nunc vel, ultricies volutpat tortor. Sed efficitur, nisl posuere molestie commodo, mauris mi lacinia orci, sit amet gravida nisl orci vel est.");
                commentDao.create(c);
            }

        } finally {
            // commit at the end
            conn.commit(savePoint);
            commentDao.endThreadConnection(conn);
        }
    }

    @Override
    public void update(long minId, long maxId) throws SQLException {

        DatabaseConnection conn = commentDao.startThreadConnection();
        Savepoint savePoint = null;
        Date now = new Date();
        try {
            savePoint = conn.setSavePoint(null);
            for(long i = minId; i<maxId; i++){
                UpdateBuilder<Comment, String> updateBuilder = commentDao.updateBuilder();
                updateBuilder.where().eq("id", i);
                updateBuilder.updateColumnValue("Text" /* column */, "Test");
                updateBuilder.update();
            }

        } finally {
            // commit at the end
            conn.commit(savePoint);
            commentDao.endThreadConnection(conn);
        }



    }

    @Override
    public void delete(long minId, long maxId) throws SQLException {
        DatabaseConnection conn = commentDao.startThreadConnection();
        Savepoint savePoint = null;
        Date now = new Date();
        try {
            savePoint = conn.setSavePoint(null);
            for(long i = minId; i<maxId; i++){
                DeleteBuilder<Comment, String> db = commentDao.deleteBuilder();
               db.where().eq("id", i);
                db.delete();
               // Log.d("ST", db.prepareStatementString());
            }

        } finally {
            // commit at the end
            conn.commit(savePoint);
            commentDao.endThreadConnection(conn);
        }
    }

    @Override
    public CommentI getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public long rowCount() throws SQLException {
        return 0;
    }
}
