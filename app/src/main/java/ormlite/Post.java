package ormlite;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import model.BadgeI;
import model.CommentI;
import model.PostI;
import model.UserI;
import model.VoteI;

@DatabaseTable(tableName = "posts")
@Data
public class Post implements PostI{
	
	@DatabaseField(columnName = "Id", id = true, generatedId = false)
	private Long id;
	
	@DatabaseField(columnName = "PostTypeId")
	private Long postTypeId;
	
	@DatabaseField(columnName = "AcceptedAnswerId")
	private Long acceptedAnswerId;
	
	@DatabaseField(columnName = "CreationDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date creationDate;
	
	@DatabaseField(columnName = "Score")
	private Integer score;
	
	@DatabaseField(columnName = "ViewCount")
	private Integer viewCount;
	
	@DatabaseField(columnName = "Body")
	private String body;

	@DatabaseField(columnName = "LastEditorDisplayName")
	private String lastEditorDisplayName;

	@DatabaseField(columnName = "OwnerDisplayName")
	private String ownerDisplayName;
	
	@DatabaseField(columnName = "LastEditDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date lastEditDate;
	
	@DatabaseField(columnName = "LastActivityDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date lastActivityDate;
	
	@DatabaseField(columnName = "Title")
	private String title;
	
	@DatabaseField(columnName = "Tags")
	private String tags;
	
	@DatabaseField(columnName = "AnswerCount")
	private Integer answerCount;
	
	@DatabaseField(columnName = "CommentCount")
	private Integer commentCount;
	
	@DatabaseField(columnName = "FavoriteCount")
	private Integer favoriteCount;
	
	@DatabaseField(columnName = "ClosedDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date closedDate;
	
	@DatabaseField(columnName = "CommunityOwnedDate",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date communityOwnedDate;
	
	@DatabaseField(columnName = "ParentId")
	private Long parentId;

	@DatabaseField(columnName = "LastEditorUserId", canBeNull = true, foreign = true)
	private User lastEditorUser;

	@DatabaseField(columnName = "OwnerUserId", canBeNull = false, foreign = true)
	private User ownerUser;
	
	@ForeignCollectionField(eager = false)
	ForeignCollection<Comment> comments;

	@ForeignCollectionField(eager = false)
	ForeignCollection<Vote> votes;


	@Override
	public UserI getOwnerUser() {
		return ownerUser;
	}

	@Override
	public void setOwnerUser(UserI ownerUser) {
		this.ownerUser = (User) ownerUser;
	}


	@Override
	public UserI getLastEditorUser() {
		return lastEditorUser;
	}

	@Override
	public void setLastEditorUser(UserI lastEditorUser) {
		this.lastEditorUser = (User)lastEditorUser;
	}


	@Override
	public List<CommentI> getComments() {
		if(comments == null) return null;

		List<CommentI> list = new ArrayList<>(comments.size());
		for(Comment c: comments){
			list.add(c);
		}
		return list;
	}


	@Override
	public List<VoteI> getVotes() {
		if(votes == null) return null;

		List<VoteI> list = new ArrayList<>(votes.size());
		for(Vote c: votes){
			list.add(c);
		}
		return list;
	}


	@Override
	public String toString() {
		String 	ownerUserS = getOwnerUser() ==null? null : getOwnerUser().getId()+"";
		String  lastEditorUserS = getLastEditorUser()== null? null : getLastEditorUser().getId()+"";

		StringBuffer sbComments = new StringBuffer();
		List<CommentI> com = getComments();
		if(com != null) {
			for (CommentI c : com) {
				sbComments.append(c.getId() + ", ");
			}
		}

		StringBuffer sbVotes = new StringBuffer();
		List<VoteI> vot = getVotes();
		if(vot != null) {
			for(VoteI v : vot) {
				sbVotes.append(v.getId()+", ");
			}
		}

		return "Post{" +
				"id=" + id +
				", postTypeId=" + postTypeId +
				", acceptedAnswerId=" + acceptedAnswerId +
				", creationDate=" + creationDate +
				", score=" + score +
				", viewCount=" + viewCount +
				", body='" + body + '\'' +
				", ownerDisplayName='" + ownerDisplayName + '\'' +
				", lastEditorDisplayName='" + lastEditorDisplayName + '\'' +
				", lastEditDate=" + lastEditDate +
				", lastActivityDate=" + lastActivityDate +
				", title='" + title + '\'' +
				", tags='" + tags + '\'' +
				", answerCount=" + answerCount +
				", commentCount=" + commentCount +
				", favoriteCount=" + favoriteCount +
				", closedDate=" + closedDate +
				", communityOwnedDate=" + communityOwnedDate +
				", parentId=" + parentId +
				", ownerUser=" + ownerUserS +
				", lastEditorUser=" + lastEditorUserS +
				", comments=[" + sbComments.toString()+"]" +
				", votes=[" + sbVotes.toString() +"]" +
				'}';
	}
}
