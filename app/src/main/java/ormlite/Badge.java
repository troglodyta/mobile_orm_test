package ormlite;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.BadgeI;
import model.UserI;

@DatabaseTable(tableName = "badges")
@Data
@ToString(exclude="user")
@EqualsAndHashCode(exclude={"user"})
public class Badge implements BadgeI {

	@DatabaseField(columnName = "Id",id = true, generatedId = false)
	private Long id;
	
	@DatabaseField(columnName = "Name")
	private String name;
	
	@DatabaseField(columnName = "Date",dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
	private Date date;
	
	@DatabaseField(columnName="Class")
	private String badgeClass;
	
	@DatabaseField(columnName = "TagBased")
	private Boolean tagBased;
	
	@DatabaseField(columnName = "UserId", canBeNull = false, foreign = true)
	private User user;

	@Override
	public UserI getUser(){
		return user;
	}

	@Override
	public void setUser(UserI user) {
		this.user = (User) user;
	}


	@Override
	public String toString() {
		String userS = getUser() == null ? null : getUser().getId()+"";
		return "Badge{" +
				"id=" + id +
				", name='" + name + '\'' +
				", date=" + date +
				", badgeClass='" + badgeClass + '\'' +
				", tagBased=" + tagBased +
				", user=" + userS +
				'}';
	}
}
