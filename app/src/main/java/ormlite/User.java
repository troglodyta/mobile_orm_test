package ormlite;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.dao.LazyForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import model.BadgeI;
import model.UserI;
import model.VoteI;


@DatabaseTable(tableName = "users")
@Data
public class User implements UserI {

    @DatabaseField(columnName ="Id", id = true, generatedId = false)
    private Long id;

    @DatabaseField(columnName = "Reputation")
    private Integer reputation;

    @DatabaseField(columnName = "CreationDate", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
    private Date creationDate;

    @DatabaseField(columnName = "DisplayName")
    private String displayName;

    @DatabaseField(columnName = "LastAccessDate", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd hh:mm:ss")
    private Date lastAccessDate;

    @DatabaseField(columnName = "WebsiteUrl")
    private String websiteUrl;

    @DatabaseField(columnName = "Location")
    private String location;

    @DatabaseField(columnName = "AboutMe")
    private String aboutMe;

    @DatabaseField(columnName = "Views")
    private String views;

    @DatabaseField(columnName = "UpVotes")
    private Integer upVotes;

    @DatabaseField(columnName = "DownVotes")
    private Integer downVotes;

    @DatabaseField(columnName = "Age")
    private Integer age;

    @DatabaseField(columnName = "AccountId")
    private Long accountId;

    @ForeignCollectionField(eager = true)
    ForeignCollection<Badge> badges;

    @Override
    public List<BadgeI> getBadges() {
        if(badges == null) return null;

        List<BadgeI> badgeIList = new ArrayList<>(badges.size());
        for(Badge b: badges){
            badgeIList.add(b);
        }
        return badgeIList;
    }


    @Override
    public String toString() {
        StringBuffer sbBadges = new StringBuffer();
        List<BadgeI> bad = getBadges();
        if(bad != null) {
            for(BadgeI b : bad) {
                sbBadges.append(b.getId()+", ");
            }
        }
        return "User{" +
                "id=" + id +
                ", reputation=" + reputation +
                ", creationDate=" + creationDate +
                ", displayName='" + displayName + '\'' +
                ", lastAccessDate=" + lastAccessDate +
                ", websiteUrl='" + websiteUrl + '\'' +
                ", location='" + location + '\'' +
                ", aboutMe='" + aboutMe + '\'' +
                ", views='" + views + '\'' +
                ", upVotes=" + upVotes +
                ", downVotes=" + downVotes +
                ", age=" + age +
                ", accountId=" + accountId +
                ", badges=[" + sbBadges.toString()+"]" +
                '}';
    }
}
