package com.example.daniel.ormtest;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import dao.CommentDaoI;
import dao.DaoManager;
import dao.PostDaoI;
import datainit.DatabaseHelper;
import datainit.SQLLiteDatabaseHelper;
import de.greenrobot.dao.query.QueryBuilder;
import greendao.BadgeDao;
import greendao.CommentDao;
import greendao.DaoMaster;
import greendao.DaoSession;
import greendao.PostDao;
import greendao.UserDao;
import greendao.VoteDao;
import model.PostI;
import ormlite.Badge;
import ormlite.Comment;
import ormlite.Post;
import ormlite.User;
import ormlite.Vote;
import test.DeleteTestCase;
import test.InstertTestCase;
import test.JoinTestCase;
import test.PostByIdTestCase;
import test.SimpleSelectTestCase;
import test.SubQueryTestCase;
import test.TestCaseI;
import test.UpdateTestCase;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView helloLabel = (TextView) findViewById(R.id.Hello);



        System.out.println("HELLO");



        Log.d("L", this.getApplicationContext().getFilesDir() + "");

            List<String> dbNames = Arrays.asList("stackoverflow_10.sqlite",
                    "stackoverflow_20.sqlite",
                    "stackoverflow_30.sqlite",
                    "stackoverflow_40.sqlite"
                    , "stackoverflow_50.sqlite"
                    , "stackoverflow_60.sqlite"
                    , "stackoverflow_70.sqlite"
                    , "stackoverflow_80.sqlite"
                    , "stackoverflow_90.sqlite"
                    , "stackoverflow_100.sqlite"
                    , "stackoverflow_200.sqlite"
                    , "stackoverflow_300.sqlite"
                    , "stackoverflow_400.sqlite"
                    , "stackoverflow_500.sqlite"
                    , "stackoverflow_600.sqlite"
                    , "stackoverflow_700.sqlite"
                    , "stackoverflow_800.sqlite"
                    , "stackoverflow_900.sqlite"
                    , "stackoverflow_1000.sqlite"
                    , "stackoverflow_2000.sqlite"
                    , "stackoverflow_3000.sqlite"
                    , "stackoverflow_4000.sqlite"
                    , "stackoverflow_5000.sqlite"
                    , "stackoverflow_6000.sqlite"
                    , "stackoverflow_7000.sqlite"
                    , "stackoverflow_8000.sqlite"
                    , "stackoverflow_9000.sqlite"
                    , "stackoverflow_10000.sqlite");
            List<DaoManager.OrmType> orms = Arrays.asList(DaoManager.OrmType.ORMLITE, DaoManager.OrmType.GREENDAO, DaoManager.OrmType.SQLLITE);

        List<String> dbMod = Arrays.asList("stackoverflow_0.sqlite");
        List<Integer> rows = Arrays.asList(10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000);
         // TestCaseI testCase = new DeleteTestCase(rows,dbMod, orms, 10, this.getApplication());
        //testCase.run();

//*******************************************************************
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        //CHANGE TESTCASE HERE!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//*******************************************************************
        TestCaseI testCase = new SubQueryTestCase(dbNames, orms, 10, this.getApplication(),false);
        testCase.run();
        testCase.saveToFile();

//        DaoManager.INSTANCE.createDaoConnection(DaoManager.OrmType.SQLLITE, "stackoverflow_10000.sqlite", this.getApplicationContext(),false);
//        PostDaoI dao = DaoManager.INSTANCE.getPostDao();
//        List<? extends  PostI> posts = null;
//        try {
//            posts = dao.threeTableJoin();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }


        helloLabel.setText(testCase.toString());

//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

       // helloLabel.setText("DONE");


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
