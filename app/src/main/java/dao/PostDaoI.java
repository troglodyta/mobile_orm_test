package dao;

import java.sql.SQLException;
import java.util.List;

import model.PostI;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface PostDaoI extends DaoI<PostI> {

    List<? extends PostI> simpleSelect() throws SQLException;

    List<? extends PostI> threeTableJoin() throws SQLException;

    List<? extends PostI> subQuery() throws SQLException;

    List<? extends PostI> limitQuery() throws SQLException;

}
