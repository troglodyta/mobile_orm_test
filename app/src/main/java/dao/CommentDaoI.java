package dao;

import java.sql.SQLException;

import model.CommentI;

/**
 * Created by Daniel on 05.05.2016.
 */
public interface CommentDaoI extends DaoI<CommentI> {

    void insert(long minId, long maxId) throws SQLException;

    void update(long minId, long maxId) throws SQLException;

    void delete(long minId, long maxId) throws SQLException;
}
