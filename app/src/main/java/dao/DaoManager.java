package dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import datainit.DatabaseHelper;
import datainit.SQLLiteDatabaseHelper;
import greendao.DaoMaster;
import greendao.DaoSession;
import greendao.dao.GreenDaoCommentDao;
import greendao.dao.GreenDaoPostDao;
import ormlite.dao.OrmLiteCommentDao;
import ormlite.dao.OrmLitePostDao;
import sqllite.dao.dao.SqlLiteCommentDao;
import sqllite.dao.dao.SqlLitePostDao;

/**
 * Created by Daniel on 03.05.2016.
 */
public class DaoManager {

    public static final DaoManager INSTANCE = new DaoManager();


    private OrmType currentOrmType;
    private String databaseName;
    private DatabaseHelper helper;
    private SQLiteDatabase db;
    private DaoSession daoSession;
    private PostDaoI postDao;
    private CommentDaoI commentDao;
    private boolean cache;
    private SQLiteDatabase sqlliteDB;

    private DaoManager(){

    };

    public void createDaoConnection(OrmType type, String databaseName, Context context, boolean cache){
        closeConnection(currentOrmType);
        this.currentOrmType = type;
        this.cache = cache;
        openConnection(type,databaseName, context);

    }

    private void openConnection(OrmType type, String databaseName, Context context){
        if(type.equals(OrmType.ORMLITE)){
            Log.d("DaoManager", "Create ORMLITE connection");
            DatabaseHelper.DATABASE_NAME = databaseName;
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            postDao = new OrmLitePostDao(helper, cache);
            commentDao = new OrmLiteCommentDao(helper, cache);
        }
        else if(type.equals(OrmType.GREENDAO)) {
            Log.d("DaoManager", "Create GreenDao connection");
            DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, databaseName, null);
            db = devOpenHelper.getWritableDatabase();
            DaoMaster daoMaster = new DaoMaster(db);
            daoSession = daoMaster.newSession();
            postDao = new GreenDaoPostDao(daoSession, cache);
            commentDao = new GreenDaoCommentDao(daoSession);
        }
        else if (type.equals(OrmType.SQLLITE)) {
            Log.d("DaoManager", "Create SQLLITE connection");
            SQLLiteDatabaseHelper helper= new SQLLiteDatabaseHelper(context, databaseName);
            sqlliteDB = helper.getWritableDatabase();
            postDao = new SqlLitePostDao(sqlliteDB);
            commentDao = new SqlLiteCommentDao(sqlliteDB);
        }


    }

    private void closeConnection(OrmType type){
        if(type == null) {

        }
        else if(type.equals(OrmType.ORMLITE)) {
            OpenHelperManager.releaseHelper();
            helper = null;
        }
        else if(type.equals(OrmType.GREENDAO)) {
            db.close();
            db = null;
            daoSession = null;
        }
        else if(type.equals(OrmType.SQLLITE)){
            sqlliteDB.close();
            sqlliteDB = null;
        }
    }


    public PostDaoI getPostDao() {
        return postDao;
    }

    public CommentDaoI getCommentDao() { return commentDao; }

    public enum OrmType {
        ORMLITE, GREENDAO, SQLLITE
    }


}
