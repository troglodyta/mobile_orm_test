package dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Daniel on 03.05.2016.
 */
public interface DaoI<T> {

    T getById(Long id) throws SQLException;
    long rowCount()throws SQLException;
//    List<T> all();
//    void delete(T entity);
//    void update(T entity);
//    void inster(T entity);
}
