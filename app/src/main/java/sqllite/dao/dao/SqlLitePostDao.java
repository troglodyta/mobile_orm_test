package sqllite.dao.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.PostDaoI;
import datainit.SQLLiteDatabaseHelper;
import model.PostI;
import sqllite.dao.Post;

/**
 * Created by Daniel on 07.05.2016.
 */
public class SqlLitePostDao implements PostDaoI {
    private SQLiteDatabase sqlliteDB;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");

    public SqlLitePostDao(SQLiteDatabase sqlliteDB){
        this.sqlliteDB = sqlliteDB;
    }

    @Override
    public List<? extends PostI> simpleSelect() throws SQLException {
        String query = "SELECT * FROM POSTS WHERE " +
                "LASTACTIVITYDATE > '2010-01-01' " +
                "AND POSTTYPEID = 1 " +
                "AND BODY LIKE '%java%'";

        Cursor c = sqlliteDB.rawQuery(query, null);
        List<PostI> posts = readListPostFromCursor(c);
        return posts;
    }

    @Override
    public List<? extends PostI> threeTableJoin() throws SQLException {
        String query = "SELECT * FROM POSTS P " +
                "JOIN USERS U ON P.LASTEDITORUSERID = U.ID " +
                "JOIN BADGES B ON B.USERID = U.ID " +
                "WHERE B.NAME = 'Scholar'";

        Cursor c = sqlliteDB.rawQuery(query, null);
        List<PostI> posts = readListPostFromCursor(c);
        return posts;
    }

    @Override
    public PostI getById(Long id) throws SQLException {
        String selectQuery = "SELECT * FROM Posts WHERE id=?";
        Cursor c = sqlliteDB.rawQuery(selectQuery, new String[]{id+""});
        PostI post = null;
        if (c.moveToFirst()) {
            try {
                post = readPostFromCursor(c);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return post;
    }

    @Override
    public long rowCount() throws SQLException {
        String query = "SELECT COUNT(*) FROM Posts;";
        Cursor cursor= sqlliteDB.rawQuery(query, null);
        cursor.moveToFirst();
        long count= cursor.getLong(0);
        return count;
    }

    public List<PostI> readListPostFromCursor(Cursor c) {
        List<PostI> posts = new ArrayList<>();
        while(c.moveToNext()) {
            try {
                PostI post = readPostFromCursor(c);
                posts.add(post);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return posts;
    }

    private PostI readPostFromCursor(Cursor c) throws ParseException{
        Post post = new Post();
        post.setId(c.getLong(c.getColumnIndex("Id")));
        post.setPostTypeId(c.getLong(c.getColumnIndex("PostTypeId")));
        post.setAcceptedAnswerId(c.getLong(c.getColumnIndex("AcceptedAnswerId")));

        post.setCreationDate(getDateFromCursor(c, "CreationDate"));
        post.setScore(c.getInt(c.getColumnIndex("Score")));
        post.setViewCount(c.getInt(c.getColumnIndex("ViewCount")));
        post.setBody(c.getString(c.getColumnIndex("Body")));
        post.setLastEditorDisplayName(c.getString(c.getColumnIndex("LastEditorDisplayName")));
        post.setOwnerDisplayName(c.getString(c.getColumnIndex("OwnerDisplayName")));

        post.setLastEditDate(getDateFromCursor(c,"LastEditDate"));

        post.setLastActivityDate(getDateFromCursor(c, "LastActivityDate"));

        post.setTitle(c.getString(c.getColumnIndex("Title")));
        post.setTags(c.getString(c.getColumnIndex("Tags")));
        post.setAnswerCount(c.getInt(c.getColumnIndex("AnswerCount")));
        post.setCommentCount(c.getInt(c.getColumnIndex("CommentCount")));
        post.setFavoriteCount(c.getInt(c.getColumnIndex("FavoriteCount")));

        post.setClosedDate(getDateFromCursor(c, "ClosedDate"));
        post.setCommunityOwnedDate(getDateFromCursor(c, "CommunityOwnedDate"));
        post.setParentId(c.getLong(c.getColumnIndex("ParentId")));
        post.setOwnerUserId(c.getLong(c.getColumnIndex("OwnerUserId")));
        post.setLastEditorUserId(c.getLong(c.getColumnIndex("LastEditorUserId")));
        return post;
    }

    private Date getDateFromCursor(Cursor c, String columnName) throws ParseException {
        String date = c.getString(c.getColumnIndex(columnName));
        return date ==null ? null : df.parse(date);
    }


    public List<? extends PostI> subQuery() throws SQLException {
        String query = "SELECT * FROM POSTS WHERE " +
                "LASTEDITORUSERID IN " +
                "(SELECT ID FROM USERS WHERE AGE > 0 AND AGE < 25)";

        Cursor c = sqlliteDB.rawQuery(query, null);
        List<PostI> posts = readListPostFromCursor(c);
        return posts;
    }

    @Override
    public List<? extends PostI> limitQuery() throws SQLException {
        return null;
    }

}
