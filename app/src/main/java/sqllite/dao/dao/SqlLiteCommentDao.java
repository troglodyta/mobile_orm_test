package sqllite.dao.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import dao.CommentDaoI;
import lombok.val;
import model.CommentI;
import ormlite.Comment;

/**
 * Created by Daniel on 08.05.2016.
 */
public class SqlLiteCommentDao implements CommentDaoI {
    private static final  String insertText = "'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quam risus, aliquam in nunc vel, ultricies volutpat tortor. Sed efficitur, nisl posuere molestie commodo, mauris mi lacinia orci, sit amet gravida nisl orci vel est.'";

    private SQLiteDatabase sqlliteDB;

    public SqlLiteCommentDao(SQLiteDatabase sqlliteDB){
        this.sqlliteDB = sqlliteDB;
    }

    @Override
    public void insert(long minId, long maxId) throws SQLException {
        sqlliteDB.beginTransaction();
        for (long i = minId; i<=maxId; i++) {
            ContentValues values = new ContentValues();
            values.put("Id", i);
            values.putNull("PostId");
            values.put("Score", "30");
            values.put("Text", insertText);
            values.put("CreationDate", "2016-05-04");
            values.putNull("UserId");
            sqlliteDB.insert("Comments", null, values);
        }
        sqlliteDB.setTransactionSuccessful();
        sqlliteDB.endTransaction();
    }

    @Override
    public void update(long minId, long maxId) throws SQLException {
        sqlliteDB.beginTransaction();
        for (long i = minId; i<=maxId; i++) {
            ContentValues values = new ContentValues();
            values.put("Text", "Test");
            sqlliteDB.update("Comments", values, "Id=" + i, null);
        }
        sqlliteDB.setTransactionSuccessful();
        sqlliteDB.endTransaction();
    }

    @Override
    public void delete(long minId, long maxId) throws SQLException {
        sqlliteDB.beginTransaction();
        StringBuffer sb = new StringBuffer();
        sb.append("Id IN (");
        for (long i = minId; i<=maxId; i++) {
            sb.append(i+",");
        }
        sb.replace(sb.length() -1,sb.length(),")");
        sqlliteDB.delete("Comments",sb.toString(),null);

        sqlliteDB.setTransactionSuccessful();
        sqlliteDB.endTransaction();
    }

    @Override
    public CommentI getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public long rowCount() throws SQLException {
        return 0;
    }

}
