package sqllite.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import model.BadgeI;
import model.UserI;


@Data
public class User implements UserI {

    private Long id;
    private Integer reputation;
    private Date creationDate;
    private String displayName;
    private Date lastAccessDate;
    private String websiteUrl;
    private String location;
    private String aboutMe;
    private String views;
    private Integer upVotes;
    private Integer downVotes;
    private Integer age;
    private Long accountId;
    private List<BadgeI> badges;



    @Override
    public String toString() {
        StringBuffer sbBadges = new StringBuffer();
        List<BadgeI> bad = getBadges();
        if(bad != null) {
            for(BadgeI b : bad) {
                sbBadges.append(b.getId()+", ");
            }
        }
        return "User{" +
                "id=" + id +
                ", reputation=" + reputation +
                ", creationDate=" + creationDate +
                ", displayName='" + displayName + '\'' +
                ", lastAccessDate=" + lastAccessDate +
                ", websiteUrl='" + websiteUrl + '\'' +
                ", location='" + location + '\'' +
                ", aboutMe='" + aboutMe + '\'' +
                ", views='" + views + '\'' +
                ", upVotes=" + upVotes +
                ", downVotes=" + downVotes +
                ", age=" + age +
                ", accountId=" + accountId +
                ", badges=[" + sbBadges.toString()+"]" +
                '}';
    }
}
