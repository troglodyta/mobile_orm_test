package sqllite.dao;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import model.CommentI;
import model.PostI;
import model.UserI;
import model.VoteI;

@Data
public class Post implements PostI{

	private Long id;
	private Long postTypeId;
	private Long acceptedAnswerId;
	private Date creationDate;
	private Integer score;
	private Integer viewCount;
	private String body;
	private String lastEditorDisplayName;
	private String ownerDisplayName;
	private Date lastEditDate;
	private Date lastActivityDate;
	private String title;
	private String tags;
	private Integer answerCount;
	private Integer commentCount;
	private Integer favoriteCount;
	private Date closedDate;
	private Date communityOwnedDate;
	private Long parentId;
	private Long ownerUserId;
	private Long lastEditorUserId;
	private UserI lastEditorUser;
	private UserI ownerUser;
	private List<CommentI> comments;
	private List<VoteI> votes;


	@Override
	public String toString() {
		String 	ownerUserS = getOwnerUser() ==null? null : getOwnerUser().getId()+"";
		String  lastEditorUserS = getLastEditorUser()== null? null : getLastEditorUser().getId()+"";

		StringBuffer sbComments = new StringBuffer();
		List<CommentI> com = getComments();
		if(com != null) {
			for (CommentI c : com) {
				sbComments.append(c.getId() + ", ");
			}
		}

		StringBuffer sbVotes = new StringBuffer();
		List<VoteI> vot = getVotes();
		if(vot != null) {
			for(VoteI v : vot) {
				sbVotes.append(v.getId()+", ");
			}
		}

		return "Post{" +
				"id=" + id +
				", postTypeId=" + postTypeId +
				", acceptedAnswerId=" + acceptedAnswerId +
				", creationDate=" + creationDate +
				", score=" + score +
				", viewCount=" + viewCount +
				", body='" + body + '\'' +
				", ownerDisplayName='" + ownerDisplayName + '\'' +
				", lastEditorDisplayName='" + lastEditorDisplayName + '\'' +
				", lastEditDate=" + lastEditDate +
				", lastActivityDate=" + lastActivityDate +
				", title='" + title + '\'' +
				", tags='" + tags + '\'' +
				", answerCount=" + answerCount +
				", commentCount=" + commentCount +
				", favoriteCount=" + favoriteCount +
				", closedDate=" + closedDate +
				", communityOwnedDate=" + communityOwnedDate +
				", parentId=" + parentId +
				", ownerUser=" + ownerUserS +
				", lastEditorUser=" + lastEditorUserS +
				", comments=[" + sbComments.toString()+"]" +
				", votes=[" + sbVotes.toString() +"]" +
				'}';
	}
}
