package sqllite.dao;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.PostI;
import model.UserI;
import model.VoteI;

@Data
@ToString(exclude="post")
@EqualsAndHashCode(exclude={"post"})
public class Vote implements VoteI{

	private Long id;
	private String voteTypeId;
	private Date creationDate;
	private UserI user;
	private PostI post;

	@Override
	public String toString() {
		String userS = user == null? null : getUser().getId()+"";
		String postS = post == null ? null : getPost().getId()+"";

		return "Vote{" +
				"id=" + id +
				", voteTypeId='" + voteTypeId + '\'' +
				", creationDate=" + creationDate +
				", user=" + userS +
				", post=" + postS +
				'}';
	}
}
