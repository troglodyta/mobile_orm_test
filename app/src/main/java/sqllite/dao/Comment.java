package sqllite.dao;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.CommentI;
import model.PostI;
import model.UserI;

@Data
@ToString(exclude="post")
@EqualsAndHashCode(exclude={"post"})
public class Comment implements CommentI {

	private Long id;
	private Integer score;
	private String text;
	private Date creationDate;
	private UserI user;
	private PostI post;

	@Override
	public String toString() {

		String userS = user == null? null : getUser().getId()+"";
		String postS = post == null ? null : getPost().getId()+"";
		return "Comment{" +
				"id=" + id +
				", score=" + score +
				", text='" + text + '\'' +
				", creationDate=" + creationDate +
				", user=" + userS +
				", post=" + postS +
				'}';
	}
}
