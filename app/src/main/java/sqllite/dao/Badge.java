package sqllite.dao;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import model.BadgeI;
import model.UserI;

@Data
@ToString(exclude="user")
@EqualsAndHashCode(exclude={"user"})
public class Badge implements BadgeI {

	private Long id;
	private String name;
	private Date date;
	private String badgeClass;
	private Boolean tagBased;
	private UserI user;


	@Override
	public String toString() {
		String userS = getUser() == null ? null : getUser().getId()+"";
		return "Badge{" +
				"id=" + id +
				", name='" + name + '\'' +
				", date=" + date +
				", badgeClass='" + badgeClass + '\'' +
				", tagBased=" + tagBased +
				", user=" + userS +
				'}';
	}
}
