package datainit;

import java.io.IOException;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Daniel on 02.05.2016.
 */
public class GreenDaoInit {

    public static void main(String[] args) {
        Schema schema = new Schema(1, "greendao");
        schema.enableKeepSectionsByDefault();

        //Badge
        Entity badge = schema.addEntity("Badge");
        Entity user = schema.addEntity("User");
        Entity post = schema.addEntity("Post");
        Entity comment = schema.addEntity("Comment");
        Entity vote = schema.addEntity("Vote");

        badge.setTableName("Badges");
        badge.addIdProperty().columnName("Id");
        badge.addStringProperty("name").columnName("Name");
        badge.addStringProperty("date").columnName("Date").customType("java.util.Date", "datainit.DateConverter");
        badge.addStringProperty("badgeClass").columnName("Class");
        badge.addBooleanProperty("tagBased").columnName("TagBased");
        Property badageUserId = badge.addLongProperty("userId").columnName("UserId").notNull().getProperty();
        badge.addToOne(user, badageUserId);


        //User
        user.setTableName("Users");
        user.addIdProperty().columnName("Id");
        user.addIntProperty("reputation").columnName("Reputation");
        user.addStringProperty("creationDate").columnName("CreationDate").customType("java.util.Date", "datainit.DateConverter");
        user.addStringProperty("displayName").columnName("DisplayName");
        user.addStringProperty("lastAccessDate").columnName("LastAccessDate").customType("java.util.Date", "datainit.DateConverter");
        user.addStringProperty("websiteUrl").columnName("WebsiteUrl");
        user.addStringProperty("location").columnName("Location");
        user.addStringProperty("aboutMe").columnName("AboutMe");
        user.addStringProperty("views").columnName("Views");
        user.addIntProperty("upVotes").columnName("UpVotes");
        user.addIntProperty("downVotes").columnName("DownVotes");
        user.addIntProperty("age").columnName("Age");
        user.addLongProperty("accountId").columnName("accountId");


        //Post
        post.setTableName("Posts");
        post.addIdProperty().columnName("Id");
        post.addLongProperty("postTypeId").columnName("PostTypeId");
        post.addLongProperty("acceptedAnswerId").columnName("AcceptedAnswerId");
        post.addStringProperty("creationDate").columnName("CreationDate").customType("java.util.Date", "datainit.DateConverter");
        post.addIntProperty("score").columnName("Score");
        post.addIntProperty("viewCount").columnName("ViewCount");
        post.addStringProperty("body").columnName("Body");
        post.addStringProperty("ownerDisplayName").columnName("OwnerDisplayName");
        post.addStringProperty("lastEditorDisplayName").columnName("LastEditorDisplayName");
        post.addStringProperty("lastEditDate").columnName("LastEditDate").customType("java.util.Date", "datainit.DateConverter");
        post.addStringProperty("lastActivityDate").columnName("LastActivityDate").customType("java.util.Date", "datainit.DateConverter");
        post.addStringProperty("title").columnName("Title");
        post.addStringProperty("Tags").columnName("Tags");
        post.addIntProperty("answerCount").columnName("AnswerCount");
        post.addIntProperty("commentCount").columnName("CommentCount");
        post.addIntProperty("favoriteCount").columnName("FavoriteCount");
        post.addStringProperty("closedDate").columnName("ClosedDate").customType("java.util.Date", "datainit.DateConverter");
        post.addStringProperty("communityOwnedDate").columnName("CommunityOwnedDate").customType("java.util.Date", "datainit.DateConverter");
        post.addLongProperty("parentId").columnName("ParentId");

        Property ownerUserId = post.addLongProperty("ownerUserId").columnName("OwnerUserId").getProperty();
        post.addToOne(user, ownerUserId).setName("ownerUser");

        Property lastEditorUserId = post.addLongProperty("lastEditorUserId").columnName("LastEditorUserId").getProperty();
        post.addToOne(user, lastEditorUserId).setName("lastEditorUser");


        //Comment
        comment.setTableName("Comments");
        comment.addIdProperty().columnName("Id");
        comment.addIntProperty("score").columnName("Score");
        comment.addStringProperty("text").columnName("Text");
        comment.addStringProperty("creationDate").columnName("CreationDate").customType("java.util.Date", "datainit.DateConverter");

        Property commentUserId = comment.addLongProperty("userId").columnName("UserId").getProperty();
        comment.addToOne(user, commentUserId).setName("user");

        Property commentPostId = comment.addLongProperty("postId").columnName("PostId").getProperty();
        comment.addToOne(post, commentPostId).setName("post");


        //Vote
        vote.setTableName("Votes");
        vote.addIdProperty().columnName("Id");
        vote.addStringProperty("voteTypeId").columnName("VoteTypeId");
        vote.addStringProperty("creationDate").columnName("CreationDate").customType("java.util.Date", "datainit.DateConverter");

        Property voteUserId = vote.addLongProperty("userId").columnName("UserId").getProperty();
        vote.addToOne(user, voteUserId).setName("user");

        Property votePostId = vote.addLongProperty("postId").columnName("PostId").getProperty();
        vote.addToOne(post, votePostId).setName("post");

        user.addToMany(badge, badageUserId).setName("badges");
        post.addToMany(comment, commentPostId).setName("comments");
        post.addToMany(vote, votePostId).setName("votes");


        DaoGenerator daoGenerator = null;
        try {
            daoGenerator = new DaoGenerator();
            daoGenerator.generateAll(schema, "app/src/main/java");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
