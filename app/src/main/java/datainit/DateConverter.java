package datainit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.greenrobot.dao.converter.PropertyConverter;

/**
 * Created by Daniel on 02.05.2016.
 */
public class DateConverter implements PropertyConverter<Date,String> {
    private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Date convertToEntityProperty(String databaseValue) {
        try {
            return format.parse(databaseValue);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String convertToDatabaseValue(Date entityProperty) {
        return format.format(entityProperty);
    }
}
