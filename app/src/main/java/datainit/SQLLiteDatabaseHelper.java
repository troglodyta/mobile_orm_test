package datainit;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Daniel on 07.05.2016.
 */
public class SQLLiteDatabaseHelper extends SQLiteOpenHelper {

    public SQLLiteDatabaseHelper(Context context, String dbName) {
        this(context,dbName,null,1);
    }

    public SQLLiteDatabaseHelper(Context context, String name,
                          SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
