package datainit;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import ormlite.Badge;
import ormlite.Comment;
import ormlite.Post;
import ormlite.User;
import ormlite.Vote;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    public static  String DATABASE_NAME    = "";
    private static final int    DATABASE_VERSION = 1;

    private Dao<User, Integer> userDao = null;
    private Dao<Post, Integer> postDao = null;
    private Dao<Comment, Integer> commentDao = null;
    private Dao<Vote, Integer> voteDao = null;
    private Dao<Badge, Integer> badgeDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {

    }

    /* User */

    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }

    public Dao<Post, Integer> getPostDao() throws SQLException {
        if (postDao == null) {
            postDao = getDao(Post.class);
        }
        return postDao;
    }

    public Dao<Comment, Integer> getCommentDao() throws SQLException {
        if (commentDao == null) {
            commentDao = getDao(Comment.class);
        }
        return commentDao;
    }

    public Dao<Vote, Integer> getVoteDao() throws SQLException {
        if (voteDao == null) {
            voteDao = getDao(Vote.class);
        }
        return voteDao;
    }

    public Dao<Badge, Integer> getBadgeDao() throws SQLException {
        if (badgeDao == null) {
            badgeDao = getDao(Badge.class);
        }
        return badgeDao;
    }

    @Override
    public void close() {
        userDao = null;
        postDao = null;
    }

    }



