package greendao.dao;

import android.util.Log;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dao.PostDaoI;
import de.greenrobot.dao.query.Join;
import de.greenrobot.dao.query.QueryBuilder;
import de.greenrobot.dao.query.WhereCondition;
import greendao.Badge;
import greendao.BadgeDao;
import greendao.DaoSession;
import greendao.Post;
import greendao.PostDao;
import greendao.User;
import greendao.UserDao;
import model.PostI;

/**
 * Created by Daniel on 03.05.2016.
 */
public class GreenDaoPostDao implements PostDaoI {

    private DaoSession daoSession;
    private PostDao postDao;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    private Date simpleSelectDate;
    private boolean cache;

    public GreenDaoPostDao(DaoSession daoSession, boolean cache) {
        this.daoSession = daoSession;
        this.cache = cache;
        postDao = daoSession.getPostDao();

        try {
            simpleSelectDate = df.parse("2010-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PostI getById(Long id) throws SQLException {
        Log.d("DaoManager", "GreenDAO");
        PostI post =postDao.queryBuilder()
            .where(PostDao.Properties.Id.eq(id))
            .unique();
        if(!cache)
            daoSession.clear();
        return post;
    }

    @Override
    public long rowCount() throws SQLException {
        return postDao.queryBuilder().count();
    }

    @Override
    public List<PostI> simpleSelect() throws SQLException {
        QueryBuilder qb = postDao.queryBuilder()
                .where(PostDao.Properties.LastActivityDate.gt("2010-01-01"),
                        PostDao.Properties.PostTypeId.eq(1),
                        PostDao.Properties.Body.like("%java%"));
        //qb.LOG_SQL = true;
        List list = qb.list();
        if(!cache)
            daoSession.clear();
        return list;
    }

    @Override
    public List<? extends PostI> threeTableJoin() throws SQLException {
        QueryBuilder qb = postDao.queryBuilder();
        Join userJoin = qb.join(PostDao.Properties.LastEditorUserId, User.class);
        Join badageJoin = qb.join(userJoin, UserDao.Properties.Id, Badge.class, BadgeDao.Properties.UserId);
        badageJoin.where(BadgeDao.Properties.Name.eq("Scholar"));
        qb.orderAsc(PostDao.Properties.CreationDate);

        //qb.LOG_SQL = true;
        List list = qb.list();
        //Log.d("ST",list.size()+"");
        if(!cache)
            daoSession.clear();
        return list;
    }

    public List<? extends PostI> subQuery() throws SQLException{
        QueryBuilder qb = postDao.queryBuilder();
        qb.where(new WhereCondition.StringCondition("LastEditorUserId IN " +
                "(SELECT ID FROM USERS WHERE AGE > 0 AND AGE < 25 )"));
        //qb.LOG_SQL = true;
        List list = qb.list();
        if(!cache)
            daoSession.clear();
        return list;
    }

    @Override
    public List<? extends PostI> limitQuery() throws SQLException {
        QueryBuilder qb = postDao.queryBuilder()
                .where(
                        PostDao.Properties.PostTypeId.eq(2)).limit(100);
        //qb.LOG_SQL = true;
        List list = qb.list();
        if(!cache)
            daoSession.clear();
        return list;
    }
}
