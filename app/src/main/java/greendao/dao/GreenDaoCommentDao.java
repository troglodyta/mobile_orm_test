package greendao.dao;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dao.CommentDaoI;
import greendao.CommentDao;
import greendao.DaoSession;
import greendao.PostDao;
import model.CommentI;
import ormlite.Comment;

/**
 * Created by Daniel on 05.05.2016.
 */
public class GreenDaoCommentDao implements CommentDaoI {
    private DaoSession daoSession;
    private CommentDao commentDao;
    private static final DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
    private Date simpleSelectDate;

    public GreenDaoCommentDao(DaoSession daoSession) {
        this.daoSession = daoSession;
        commentDao = daoSession.getCommentDao();
        try {
            simpleSelectDate = df.parse("2010-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }



    @Override
    public void insert(long minId, long maxId) throws SQLException {
        ArrayList<greendao.Comment> comments = new ArrayList<greendao.Comment>((int)(maxId - minId + 5));
        Date now = new Date();
        for(long i = minId; i<maxId; i++) {
            greendao.Comment c = new greendao.Comment();
            c.setId(i);
            c.setCreationDate(now);
            c.setScore(30);
            c.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quam risus, aliquam in nunc vel, ultricies volutpat tortor. Sed efficitur, nisl posuere molestie commodo, mauris mi lacinia orci, sit amet gravida nisl orci vel est.");
            comments.add(c);
        }

        commentDao.insertInTx(comments);

    }

    @Override
    public void update(long minId, long maxId) throws SQLException {
        ArrayList<greendao.Comment> comments = new ArrayList<greendao.Comment>((int)(maxId - minId + 5));
        Date now = new Date();


        for(long i = minId; i<maxId; i++) {
            greendao.Comment c = new greendao.Comment();
            c.setId(i);
            c.setCreationDate(now);
            c.setScore(30);
            c.setText("Test");
            comments.add(c);
        }
        commentDao.updateInTx(comments);
    }

    @Override
    public void delete(long minId, long maxId) throws SQLException {
        List<Long> longs = new ArrayList<>((int)(maxId - minId)+5);
        for(long i = minId; i<maxId; i++) {
            longs.add(i);
        }
        commentDao.queryBuilder().LOG_SQL = true;
        commentDao.deleteByKeyInTx(longs);
    }

    @Override
    public CommentI getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public long rowCount() throws SQLException {
        return 0;
    }
}
