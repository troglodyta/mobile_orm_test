package greendao;

import java.util.ArrayList;
import java.util.List;
import greendao.DaoSession;
import de.greenrobot.dao.DaoException;
import model.BadgeI;
import model.CommentI;
import model.PostI;
import model.UserI;
import model.VoteI;

import java.util.Date;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table "Posts".
 */
public class Post implements PostI{

    private Long id;
    private Long postTypeId;
    private Long acceptedAnswerId;
    private Date creationDate;
    private Integer score;
    private Integer viewCount;
    private String body;
    private String ownerDisplayName;
    private String lastEditorDisplayName;
    private Date lastEditDate;
    private Date lastActivityDate;
    private String title;
    private String Tags;
    private Integer answerCount;
    private Integer commentCount;
    private Integer favoriteCount;
    private Date closedDate;
    private Date communityOwnedDate;
    private Long parentId;
    private Long ownerUserId;
    private Long lastEditorUserId;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient PostDao myDao;

    private User ownerUser;
    private Long ownerUser__resolvedKey;

    private User lastEditorUser;
    private Long lastEditorUser__resolvedKey;

    private List<Comment> comments;
    private List<Vote> votes;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Post() {
    }

    public Post(Long id) {
        this.id = id;
    }

    public Post(Long id, Long postTypeId, Long acceptedAnswerId, Date creationDate, Integer score, Integer viewCount, String body, String ownerDisplayName, String lastEditorDisplayName, Date lastEditDate, Date lastActivityDate, String title, String Tags, Integer answerCount, Integer commentCount, Integer favoriteCount, Date closedDate, Date communityOwnedDate, Long parentId, Long ownerUserId, Long lastEditorUserId) {
        this.id = id;
        this.postTypeId = postTypeId;
        this.acceptedAnswerId = acceptedAnswerId;
        this.creationDate = creationDate;
        this.score = score;
        this.viewCount = viewCount;
        this.body = body;
        this.ownerDisplayName = ownerDisplayName;
        this.lastEditorDisplayName = lastEditorDisplayName;
        this.lastEditDate = lastEditDate;
        this.lastActivityDate = lastActivityDate;
        this.title = title;
        this.Tags = Tags;
        this.answerCount = answerCount;
        this.commentCount = commentCount;
        this.favoriteCount = favoriteCount;
        this.closedDate = closedDate;
        this.communityOwnedDate = communityOwnedDate;
        this.parentId = parentId;
        this.ownerUserId = ownerUserId;
        this.lastEditorUserId = lastEditorUserId;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPostDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostTypeId() {
        return postTypeId;
    }

    public void setPostTypeId(Long postTypeId) {
        this.postTypeId = postTypeId;
    }

    public Long getAcceptedAnswerId() {
        return acceptedAnswerId;
    }

    public void setAcceptedAnswerId(Long acceptedAnswerId) {
        this.acceptedAnswerId = acceptedAnswerId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOwnerDisplayName() {
        return ownerDisplayName;
    }

    public void setOwnerDisplayName(String ownerDisplayName) {
        this.ownerDisplayName = ownerDisplayName;
    }

    public String getLastEditorDisplayName() {
        return lastEditorDisplayName;
    }

    public void setLastEditorDisplayName(String lastEditorDisplayName) {
        this.lastEditorDisplayName = lastEditorDisplayName;
    }

    public Date getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(Date lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public Date getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(Date lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTags() {
        return Tags;
    }

    public void setTags(String Tags) {
        this.Tags = Tags;
    }

    public Integer getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(Integer answerCount) {
        this.answerCount = answerCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(Integer favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public Date getCommunityOwnedDate() {
        return communityOwnedDate;
    }

    public void setCommunityOwnedDate(Date communityOwnedDate) {
        this.communityOwnedDate = communityOwnedDate;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public Long getLastEditorUserId() {
        return lastEditorUserId;
    }

    public void setLastEditorUserId(Long lastEditorUserId) {
        this.lastEditorUserId = lastEditorUserId;
    }

    /** To-one relationship, resolved on first access. */
    public UserI getOwnerUser() {
        Long __key = this.ownerUserId;
        if (ownerUser__resolvedKey == null || !ownerUser__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            User ownerUserNew = targetDao.load(__key);
            synchronized (this) {
                ownerUser = ownerUserNew;
            	ownerUser__resolvedKey = __key;
            }
        }
        return ownerUser;
    }

    public void setOwnerUser(UserI ownerUser) {
        synchronized (this) {
            this.ownerUser = (User)ownerUser;
            ownerUserId = ownerUser == null ? null : ownerUser.getId();
            ownerUser__resolvedKey = ownerUserId;
        }
    }

    /** To-one relationship, resolved on first access. */
    public UserI getLastEditorUser() {
        Long __key = this.lastEditorUserId;
        if (lastEditorUser__resolvedKey == null || !lastEditorUser__resolvedKey.equals(__key)) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            UserDao targetDao = daoSession.getUserDao();
            User lastEditorUserNew = targetDao.load(__key);
            synchronized (this) {
                lastEditorUser = lastEditorUserNew;
            	lastEditorUser__resolvedKey = __key;
            }
        }
        return lastEditorUser;
    }

    public void setLastEditorUser(UserI lastEditorUser) {
        synchronized (this) {
            this.lastEditorUser = (User)lastEditorUser;
            lastEditorUserId = lastEditorUser == null ? null : lastEditorUser.getId();
            lastEditorUser__resolvedKey = lastEditorUserId;
        }
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<CommentI> getComments() {
        if (comments == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CommentDao targetDao = daoSession.getCommentDao();
            List<Comment> commentsNew = targetDao._queryPost_Comments(id);
            synchronized (this) {
                if(comments == null) {
                    comments = commentsNew;
                }
            }
        }

        List<CommentI> c = new ArrayList<>(comments.size());
        c.addAll(comments);
        return c;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetComments() {
        comments = null;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<VoteI> getVotes() {
        if (votes == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            VoteDao targetDao = daoSession.getVoteDao();
            List<Vote> votesNew = targetDao._queryPost_Votes(id);
            synchronized (this) {
                if(votes == null) {
                    votes = votesNew;
                }
            }
        }

        List<VoteI> b = new ArrayList<>(votes.size());
        b.addAll(votes);
        return b;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetVotes() {
        votes = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here
    @Override
    public String toString() {
        String 	ownerUserS = getOwnerUser() ==null? null : getOwnerUser().getId()+"";
        String  lastEditorUserS = getLastEditorUser()== null? null : getLastEditorUser().getId()+"";

        StringBuffer sbComments = new StringBuffer();
        List<CommentI> com = getComments();
        if(com != null) {
            for (CommentI c : com) {
                sbComments.append(c.getId() + ", ");
            }
        }

        StringBuffer sbVotes = new StringBuffer();
        List<VoteI> vot = getVotes();
        if(vot != null) {
            for(VoteI v : vot) {
                sbVotes.append(v.getId()+", ");
            }
        }

        return "Post{" +
                "id=" + id +
                ", postTypeId=" + postTypeId +
                ", acceptedAnswerId=" + acceptedAnswerId +
                ", creationDate=" + creationDate +
                ", score=" + score +
                ", viewCount=" + viewCount +
                ", body='" + body + '\'' +
                ", ownerDisplayName='" + ownerDisplayName + '\'' +
                ", lastEditorDisplayName='" + lastEditorDisplayName + '\'' +
                ", lastEditDate=" + lastEditDate +
                ", lastActivityDate=" + lastActivityDate +
                ", title='" + title + '\'' +
                ", tags='" + Tags + '\'' +
                ", answerCount=" + answerCount +
                ", commentCount=" + commentCount +
                ", favoriteCount=" + favoriteCount +
                ", closedDate=" + closedDate +
                ", communityOwnedDate=" + communityOwnedDate +
                ", parentId=" + parentId +
                ", ownerUser=" + ownerUserS +
                ", lastEditorUser=" + lastEditorUserS +
                ", comments=[" + sbComments.toString()+"]" +
                ", votes=[" + sbVotes.toString() +"]" +
                '}';
    }
    // KEEP METHODS END

}
