package greendao;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import datainit.DateConverter;
import java.util.Date;

import greendao.Vote;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "Votes".
*/
public class VoteDao extends AbstractDao<Vote, Long> {

    public static final String TABLENAME = "Votes";

    /**
     * Properties of entity Vote.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "Id");
        public final static Property VoteTypeId = new Property(1, String.class, "voteTypeId", false, "VoteTypeId");
        public final static Property CreationDate = new Property(2, String.class, "creationDate", false, "CreationDate");
        public final static Property UserId = new Property(3, Long.class, "userId", false, "UserId");
        public final static Property PostId = new Property(4, Long.class, "postId", false, "PostId");
    };

    private DaoSession daoSession;

    private final DateConverter creationDateConverter = new DateConverter();
    private Query<Vote> post_VotesQuery;

    public VoteDao(DaoConfig config) {
        super(config);
    }
    
    public VoteDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"Votes\" (" + //
                "\"Id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"VoteTypeId\" TEXT," + // 1: voteTypeId
                "\"CreationDate\" TEXT," + // 2: creationDate
                "\"UserId\" INTEGER," + // 3: userId
                "\"PostId\" INTEGER);"); // 4: postId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"Votes\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Vote entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String voteTypeId = entity.getVoteTypeId();
        if (voteTypeId != null) {
            stmt.bindString(2, voteTypeId);
        }
 
        Date creationDate = entity.getCreationDate();
        if (creationDate != null) {
            stmt.bindString(3, creationDateConverter.convertToDatabaseValue(creationDate));
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(4, userId);
        }
 
        Long postId = entity.getPostId();
        if (postId != null) {
            stmt.bindLong(5, postId);
        }
    }

    @Override
    protected void attachEntity(Vote entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Vote readEntity(Cursor cursor, int offset) {
        Vote entity = new Vote( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // voteTypeId
            cursor.isNull(offset + 2) ? null : creationDateConverter.convertToEntityProperty(cursor.getString(offset + 2)), // creationDate
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3), // userId
            cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4) // postId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Vote entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setVoteTypeId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setCreationDate(cursor.isNull(offset + 2) ? null : creationDateConverter.convertToEntityProperty(cursor.getString(offset + 2)));
        entity.setUserId(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
        entity.setPostId(cursor.isNull(offset + 4) ? null : cursor.getLong(offset + 4));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Vote entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Vote entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    /** Internal query to resolve the "votes" to-many relationship of Post. */
    public List<Vote> _queryPost_Votes(Long postId) {
        synchronized (this) {
            if (post_VotesQuery == null) {
                QueryBuilder<Vote> queryBuilder = queryBuilder();
                queryBuilder.where(Properties.PostId.eq(null));
                post_VotesQuery = queryBuilder.build();
            }
        }
        Query<Vote> query = post_VotesQuery.forCurrentThread();
        query.setParameter(0, postId);
        return query.list();
    }

    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getUserDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getPostDao().getAllColumns());
            builder.append(" FROM Votes T");
            builder.append(" LEFT JOIN Users T0 ON T.\"UserId\"=T0.\"Id\"");
            builder.append(" LEFT JOIN Posts T1 ON T.\"PostId\"=T1.\"Id\"");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Vote loadCurrentDeep(Cursor cursor, boolean lock) {
        Vote entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        User user = loadCurrentOther(daoSession.getUserDao(), cursor, offset);
        entity.setUser(user);
        offset += daoSession.getUserDao().getAllColumns().length;

        Post post = loadCurrentOther(daoSession.getPostDao(), cursor, offset);
        entity.setPost(post);

        return entity;    
    }

    public Vote loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Vote> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Vote> list = new ArrayList<Vote>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Vote> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Vote> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}
